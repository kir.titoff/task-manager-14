package ru.t1.ktitov.tm.api.service;

import ru.t1.ktitov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
