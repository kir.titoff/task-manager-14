package ru.t1.ktitov.tm.api.model;

import ru.t1.ktitov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
